Виконував Христіан Бусигін (FS4_ONLINE) в період з 14/05/23 по 24/05/23.
-

Під час написання проєкту я використовував реляційні бази данних на хостингу neon (https://neon.tech/) з трьома таблицями, а саме:

| liked_users | messages            | users     |
|-------------|---------------------|-----------|
| uid_b       | message_id          | uid       |
| uid_w       | sender_id           | username  |
|             | receiver_id         | photo_uri |
|             | message_text        | email     |
|             | timestamp           | password  |

user:uid пов'язаний там де його використовують, ось мій query для створення бд:

CREATE TABLE users (
uid SERIAL PRIMARY KEY,
username text,
photo_uri text,
email text UNIQUE,
password text
);

CREATE TABLE liked_users (
uid_b int,
uid_w int,
CONSTRAINT unique_like_pair UNIQUE (uid_b, uid_w)
);

CREATE TABLE messages (
message_id SERIAL PRIMARY KEY,
sender_id   int references users(uid),
receiver_id int references users(uid),
message_text text,
timestamp timestamp with time zone DEFAULT current_timestamp
);

____________
Оскільки я працював сам, то гітом я не користвувався так як довіряю своїй машині, будучи в проєкті з реальними людьми я б створив свою гілку, пулив би роботу моїх колег та пушив би свою.
_____________
Я допускаю що чогось не знаю, але на скільки мені відомо хероку - платний хостинг, я вже ,на жаль, туди деплоїв мою фіналку фронту, і з мене зтягнули грошики, дозволити собі я не можу платити за деплой на хероку, але якщо там є безкоштовний план я з радістю закину цей проєкт та спробую перездати його на вищий бал якщо це можливо.
_____________

Нижче наведені профілі, адреса та паролі, для зручного тестування з різних боків

|  Профіль  | Адреса                     | Пароль       |
|-----------|----------------------------|--------------|
|  Walter   | heisenberg@example.com     | BlueSky123   |
|  Tyler    | tyler.d@example.com        | FightClub99  |
|  Patrick  | americanpsycho@example.com | Chainsaw1    |
|  Travis   | travisb@example.com        | LonelyN1ghts |

гарного дня!