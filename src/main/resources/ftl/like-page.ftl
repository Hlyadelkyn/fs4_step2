<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">

    <title>Like page</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
          crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="static/css/style.css">
</head>
<body style="background-color: #f5f5f5;">
<section class="container-custom">

<div class="col-4 offset-4">
    <form id="form" action="http://localhost:8080/users?id=" method="post">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 text-center ">
                        <img src="${imgSrc}" class="photo-fix" >
                        <h3 class="mb-0 text-truncated name" id="u-name">${name}</h3>
                        <span id="uid">${id}</span>
                        <br>
                    </div>
                    <div class="col-12 col-lg-6">
                        <button id="noBTN"  class="btn btn-outline-danger btn-block" type="submit" name="choice" value="NO"><span class="fa fa-times"></span> Dislike</button>
                    </div>
                    <div class="col-12 col-lg-6">
                        <button id="yesBTN" class="btn btn-outline-success btn-block" type="submit" name="choice" value="YES" ><span class="fa fa-heart"></span> Like</button>
                    </div>
                </div>

            </div>

        </div>
    </form>
</div>
<script src="static/js/like-page.js"></script>
</section>
</body>
</html>
