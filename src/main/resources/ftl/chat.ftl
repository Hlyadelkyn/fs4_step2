<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">

    <title>Chat</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous">

    <link href="static/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="static/css/style.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="chat-main col-6 offset-3">
            <div class="col-md-12 chat-header">
                <div class="row header-one text-white p-1">
                    <div class="col-md-6 name pl-2">
                        <i class="fa fa-comment"></i>
                        <h6 class="ml-1 mb-0">${c.name} | id: <span id="chat-uid">${c.getId()}</span></h6>
                    </div>
                    <div class="col-md-6 options text-right pr-0">
                        <i class="fa fa-window-minimize hide-chat-box hover text-center pt-1"></i>
                        <p class="arrow-up mb-0">
                            <i class="fa fa-arrow-up text-center pt-1"></i>
                        </p>
                        <i class="fa fa-times hover text-center pt-1"></i>
                    </div>
                </div>
                <div class="row header-two w-100">
                    <div class="col-md-6 options-left pl-1">
                        <i class="fa fa-video-camera mr-3"></i>
                        <i class="fa fa-user-plus"></i>
                    </div>
                    <div class="col-md-6 options-right text-right pr-2">
                        <i class="fa fa-cog"></i>
                    </div>
                </div>
            </div>
            <div class="chat-content">
                <div class="col-md-12 chats pt-3 pl-2 pr-3 pb-3">
                <#if (messages)??>
                 <#list messages as m>
                    <ul class="p-0">

                        <#if m.sentId == lid>
                        <li class="send-msg float-right mb-2">
                          <p class="pt-1 pb-1 pl-2 pr-2 m-0 rounded">
                          ${m.text}
                          </p>
                        </li>
                        <#else>
                        <li class="receive-msg float-left mb-2">

                            <div class="receive-msg-desc  ml-2">
                              <p class="bg-white m-0 pt-1 pb-1 pl-2 pr-2 rounded">
                               ${m.text}
                              </p>
                            </div>
                            <div class="sender-img float-left">
                                                           <img src="${c.photoWebURL}">
                                                        </div>
                          </li>
                        </#if>
                    </ul>
                 </#list>
                 <#else>
                 no messages
                 </#if>
                </div>
                <div class="col-md-12 p-2 msg-box border border-primary">
                    <div class="row">
                        <div class="col-md-2 options-left">
                            <i class="fa fa-smile-o"></i>
                        </div>
                        <div class="col-md-7 pl-0">
                        <form id="chat-form" action="http://localhost:8080/messages/?chatmateid=" method="post">
                            <input type="text" id="chat-input" class="border-0" placeholder=" Send message" />
                        </form>
                        <button id="chat-sbm-btn" class="btn btn-outline-success btn-block" onclick="compileDynamicURL">Send</button>
                        </div>
                        <div class="col-md-3 text-right options-right">
                            <i class="fa fa-picture-o mr-2"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="static/js/chat.js"></script>
</body>
</html>
