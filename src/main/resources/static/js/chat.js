function compileDynamicURL(){
    let compiledURL;
    let msg     = document.getElementById("chat-input").value;
    let chmtID  = document.querySelector("#chat-uid").textContent;
    compiledURL = document.getElementById("chat-form").action + chmtID+"&text="+msg;
    document.getElementById("chat-form").action = compiledURL;
    document.getElementById("chat-form").submit();
}

let btn = document.querySelector("#chat-sbm-btn");
btn.addEventListener("click", compileDynamicURL);
