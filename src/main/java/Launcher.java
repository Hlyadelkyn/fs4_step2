import org.controllers.SQLDAO;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.servlets.*;
import org.utils.CookieFilter;
import sql.DB;
import javax.servlet.DispatcherType;

import java.sql.Connection;
import java.util.EnumSet;
import java.util.Optional;

public class Launcher {

    static Optional<Integer> toInt(String raw) {
        try {
            return Optional.of(Integer.parseInt(raw));
        } catch (Exception x) {
            return Optional.empty();
        }
    }

    public static void main(String[] args) throws Exception {

        Connection conn = DB.conn();

        SQLDAO            sdao = new SQLDAO(conn);

        ProfilesServlet   usersServlet = new ProfilesServlet(   sdao);
        LikedServlet      likedServlet = new LikedServlet(      sdao);
        ChatServlet       chatServlet  = new ChatServlet(       sdao);
		LoginServlet      loginServlet = new LoginServlet(      sdao, usersServlet);

        CookieFilter     cookiesFilter = new CookieFilter();
        EnumSet<DispatcherType> dt = EnumSet.of(DispatcherType.REQUEST);


        ServletContextHandler handler = new ServletContextHandler();

        Integer port = Optional.ofNullable(System.getenv("PORT")).flatMap(Launcher::toInt).orElse(8080);
        Server server = new Server(port);

        handler.setSessionHandler(new SessionHandler());

        String staticDir = Launcher.class.getClassLoader().getResource("static").toURI().getPath();


        handler.addServlet( new ServletHolder(usersServlet),                          "/users");
        handler.addServlet( new ServletHolder(likedServlet),                          "/liked");
        handler.addServlet( new ServletHolder(chatServlet),                           "/messages/*");
        handler.addServlet( new ServletHolder(new StaticResourcesServlet(staticDir)), "/messages/static/*");
        handler.addServlet( new ServletHolder(new StaticResourcesServlet(staticDir)), "/static/*");
		handler.addServlet( new ServletHolder(loginServlet),                          "/login");

        handler.addFilter(new FilterHolder(cookiesFilter), "/users", dt);
        handler.addFilter(new FilterHolder(cookiesFilter), "/liked", dt);
        handler.addFilter(new FilterHolder(cookiesFilter), "/messages/*", dt);

        server.setHandler(handler);

        server.start();
        server.join();
    }

}
