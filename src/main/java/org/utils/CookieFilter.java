package org.utils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.function.Function;

public class CookieFilter implements Filter  {
    @Override
    public void init(FilterConfig filterConfig) {

    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        Cookie[] cookies = request.getCookies();
        boolean isCookiePresent = false;

        if (cookies != null){
            for (Cookie ck : cookies) {
                if ("uid".equals(ck.getName())) {
                    HttpSession session = request.getSession();
                    session.setMaxInactiveInterval(60*25);
                    session.setAttribute("uid", ck.getValue());
                    isCookiePresent = true;
                }
            }
            if(isCookiePresent){
                chain.doFilter(request, res);
            }else{
                response.sendRedirect("/login");
            }

        }else{
            response.sendRedirect("/login");
        }
}

    @Override
    public void destroy() {

    }
}
