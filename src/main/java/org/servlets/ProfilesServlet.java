package org.servlets;

import lombok.SneakyThrows;

import freemarker.template.Configuration;
import org.controllers.SQLDAO;
import org.entities.Profile;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Objects;

public class ProfilesServlet extends HttpServlet {
    SQLDAO SDAO;
    Profile profile;
    int count = 0;
    int loggedID;

    public ProfilesServlet(SQLDAO s) {
        SDAO = s;
    }

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        loggedID = Integer.parseInt(req.getSession().getAttribute("uid").toString());

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
        cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        cfg.setDirectoryForTemplateLoading(new File(getClass().getClassLoader().getResource("ftl").toURI().getPath()));

        if (SDAO.getUsersCount() == count) {
            resp.sendRedirect(req.getContextPath() + "/liked");
            count = 0;
            SDAO.setLastUserId(0);
        } else {
            count++;
            profile = SDAO.getNext();

            HashMap<String, Object> data = new HashMap<>();
            if (profile.getId() == loggedID) {
                data.put("name", profile.getName() + "(you)");
            } else {
                data.put("name", profile.getName());
            }
            data.put("imgSrc", profile.getPhotoWebURL());
            data.put("id", profile.getId());
            try (PrintWriter w = resp.getWriter()) {
                cfg.getTemplate("like-page.ftl").process(data, w);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String choiceS = req.getParameter("choice");     //YES / NO
        boolean choice = Objects.equals(choiceS, "YES"); //BOOL
        String id = req.getParameter("id");
        try {
            if (choice) {
                int parsed = Integer.parseInt(id);
                SDAO.likeUser(loggedID, parsed);
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        resp.sendRedirect(req.getContextPath() + "/users");
    }

    public void setCount(int count) {
        this.count = count;
    }
}
