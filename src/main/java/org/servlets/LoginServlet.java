package org.servlets;

import freemarker.template.Configuration;
import lombok.SneakyThrows;
import org.controllers.ProfilesDB;
import org.controllers.SQLDAO;
import org.entities.Profile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class LoginServlet extends HttpServlet {
	SQLDAO SDAO;
	ProfilesServlet PS;
	public LoginServlet(SQLDAO s, ProfilesServlet p){
		SDAO = s;
		PS = p;
	}

	@SneakyThrows
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
		cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
		cfg.setDirectoryForTemplateLoading(new File(getClass().getClassLoader().getResource("ftl").toURI().getPath()));
		HashMap<String, Object> data = new HashMap<>();

		try (PrintWriter w = resp.getWriter()) {
			cfg.getTemplate("login.ftl").process(data, w);
		}
	}
	@SneakyThrows
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp){

		String ps = req.getParameter("password");
		String e = req.getParameter("email");
		Integer id = SDAO.LogIn(e,ps);
		if(id!=null){
			Profile p = SDAO.getUserById(id);

			Cookie idc = new Cookie("uid", String.valueOf(p.getId()));
			idc.setMaxAge(60*25);
			resp.addCookie(idc);

			SDAO.setLastUserId(0);
			PS.setCount(0);

			resp.sendRedirect("/users");
		}else{
			resp.sendRedirect("/login");
		}
	}
}
