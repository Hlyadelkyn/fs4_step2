package org.servlets;

import freemarker.template.Configuration;
import lombok.SneakyThrows;
import org.controllers.ProfilesDAO;
import org.controllers.SQLDAO;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;

public class LikedServlet extends HttpServlet {
	SQLDAO      SDAO;
	int         loggedID;

	public LikedServlet( SQLDAO d) {
		SDAO      = d;
	}

	@SneakyThrows
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		loggedID = Integer.parseInt(req.getSession().getAttribute("uid").toString());

		Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
		cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
		cfg.setDirectoryForTemplateLoading(new File(getClass().getClassLoader().getResource("ftl").toURI().getPath()));

		HashMap<String, Object> data = new HashMap<>();

		data.put("likedUsers",SDAO.getLikedProfiles(loggedID).toArray());

		try (PrintWriter w = resp.getWriter()) {
//			System.out.println(Arrays.toString(SDAO.getLikedProfiles(loggedID).toArray()));
			cfg.getTemplate("people-list.ftl").process(data, w);
		}
	}
}
