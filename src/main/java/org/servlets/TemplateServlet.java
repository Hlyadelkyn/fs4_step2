package org.servlets;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;


public class TemplateServlet extends HttpServlet {
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
		cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
		cfg.setDirectoryForTemplateLoading(new File(Dir.get("flt")));
	}
}
