package org.servlets;
import java.net.URISyntaxException;

public class Dir {
	public static String get(String dir) {
		try {
			return Dir.class
					.getClassLoader()
					.getResource(dir)
					.toURI()
					.getPath();
		} catch (URISyntaxException e) {
			throw new RuntimeException(String.format("Requested path `%s`not found", dir), e);
		}
	}

}
