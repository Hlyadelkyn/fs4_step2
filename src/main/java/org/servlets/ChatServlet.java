package org.servlets;

import freemarker.template.Configuration;
import lombok.SneakyThrows;
import org.controllers.SQLDAO;
import org.entities.Message;
import org.entities.Profile;
import org.controllers.ProfilesDAO;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class ChatServlet extends HttpServlet {
	SQLDAO      SDAO;
	int loggedId;

	public ChatServlet( SQLDAO s){
		SDAO = s;
	}
	@SneakyThrows
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		loggedId = Integer.parseInt(req.getSession().getAttribute("uid").toString());

		Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
		cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
		cfg.setDirectoryForTemplateLoading(new File(getClass().getClassLoader().getResource("ftl").toURI().getPath()));

		String pathInfo = req.getPathInfo();
		if (pathInfo.startsWith("/")) pathInfo = pathInfo.substring(1);


		try {
			int parsedContextPath = Integer.parseInt(pathInfo);
			Profile bdReq = SDAO.getUserById(parsedContextPath);
			if(bdReq != null){
				HashMap<String, Object> data = new HashMap<>();

				ArrayList<Message> contextMessages = SDAO.getMessagesBy2ID(loggedId, parsedContextPath);
				data.put("messages",contextMessages);
				data.put("lid", loggedId);
				data.put("c", SDAO.getUserById(parsedContextPath));

				try (PrintWriter w = resp.getWriter()) {
					cfg.getTemplate("chat.ftl").process(data, w);
				}
			}else {
				resp.getWriter().println("NOT FOUND 404");
			}

		}catch (Exception ex){
			resp.getWriter().println("BAD REQUEST 400, detailed: Not A Number");
		}
	}

	@SneakyThrows
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp){

		String chatmateids = req.getParameter("chatmateid");
		String text        = req.getParameter("text");

		System.out.println(chatmateids+ " | "+text);
		try {
			int parsedID = Integer.parseInt(chatmateids);
			SDAO.addMessage(loggedId,parsedID,text);
		}catch (Exception ex){
			System.out.println(ex);
		}

		resp.sendRedirect(req.getContextPath() + "/messages/"+chatmateids);
	}
}
