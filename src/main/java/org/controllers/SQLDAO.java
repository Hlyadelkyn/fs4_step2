package org.controllers;

import org.entities.Message;
import org.entities.Profile;
import sql.DBUtils;

import java.sql.*;
import java.util.ArrayList;

public class SQLDAO {
	Connection conn;
	Profile profile = null;

	ArrayList<Message> msgs = new ArrayList<>();
	public int lastUserId = 0;

	public SQLDAO(Connection connection){
		conn = connection;
	}
	public void               addUser(Profile profileToAdd){
		try{
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO users (username, photo_uri,email, password) values (?, ?,?,?)");
			stmt.setString(1, profileToAdd.getName());
			stmt.setString(2, profileToAdd.getPhotoWebURL());
			stmt.setString(3, profileToAdd.getEmail());
			stmt.setString(4, profileToAdd.getPassword());
			stmt.executeUpdate();
		}catch (Exception ex){
			System.out.println(ex);
		}
	}
	public void               likeUser(int b, int w){
		try {
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO liked_users (uid_b, uid_w) values (?, ?)");
			stmt.setInt(1, b);
			stmt.setInt(2, w);
			stmt.executeUpdate();

		}catch (Exception ex){
			System.out.println(ex);
		}
	}

	public Profile            getUserById(int id){
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM users WHERE uid = ?");
			stmt.setInt(1, id);
			ResultSet profileResult = stmt.executeQuery();

			if (profileResult.next()) {
				String name = profileResult.getString("username");
				String photoWebURL = profileResult.getString("photo_uri");
				profile = new Profile( name, photoWebURL, id);
			}


		}catch (Exception ex){
			System.out.println(ex);
			System.out.println("_________________________________________________________________");
		}
		return profile;
	}
	public ArrayList<Profile> getLikedProfiles(int id){
		ArrayList<Profile> likedProfiles = new ArrayList<>();
		try {
			String query = "SELECT * FROM liked_users WHERE uid_b = ?";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int likedUserId = resultSet.getInt("uid_w");
//				System.out.println(likedUserId);
				Profile likedProfile = getUserById(likedUserId);
				likedProfiles.add(likedProfile);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
		return likedProfiles;
	}
	public void               addMessage(int sid, int rid, String text){
		try {
			PreparedStatement stmt = conn.prepareStatement( "INSERT INTO messages (sender_id, receiver_id, message_text) VALUES (?, ?, ?)");
			stmt.setInt(   1, sid);
			stmt.setInt(   2, rid);
			stmt.setString(3, text);
			stmt.executeUpdate();


		}catch (Exception ex){
			System.out.println(ex);
		}
	}
	public ArrayList<Message> getMessagesBy2ID(int id1, int id2){
		msgs.clear();
		try {
			String messagesQuery = "SELECT * FROM messages WHERE (sender_id = ? AND receiver_id = ?) OR (sender_id = ? AND receiver_id = ?)";
			PreparedStatement mstmt = conn.prepareStatement(messagesQuery);
			mstmt.setInt(1, id1);
			mstmt.setInt(2, id2);
			mstmt.setInt(3, id2);
			mstmt.setInt(4, id1);

			ResultSet res = mstmt.executeQuery();
			while (res.next()) {
				int messageId       = res.getInt("message_id");
				int senderId        = res.getInt("sender_id");
				int receiverId      = res.getInt("receiver_id");
				String text         = res.getString("message_text");
				Timestamp timestamp = res.getTimestamp("timestamp");

				Message message = new Message(messageId, senderId, receiverId, text, timestamp);
				msgs.add(message);
			}


		}catch (Exception ex){
			System.out.println(ex);
		}

		return msgs;
	}
	public Profile            getNext(){
		Profile nextUser = null;
		String query;
		if (lastUserId == 0) {
			query = "SELECT * FROM users ORDER BY uid ASC LIMIT 1";
		} else {
			System.out.println(lastUserId);
			query = "SELECT * FROM users WHERE uid > ? ORDER BY uid ASC LIMIT 1";
		}
		try (PreparedStatement statement = conn.prepareStatement(query)) {
			if (lastUserId != 0) {
				statement.setInt(1, lastUserId);
			}
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				int userId = resultSet.getInt("uid");
				String username = resultSet.getString("username");
				String photoUri = resultSet.getString("photo_uri");

				nextUser = new Profile( username, photoUri,userId );
				lastUserId = userId;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nextUser;

	}
	public Profile            getFirst(){
		Profile user = null;
		String query;
		query = "SELECT * FROM users ORDER BY uid ASC LIMIT 1";

		try(PreparedStatement statement = conn.prepareStatement(query)) {
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				int userId = resultSet.getInt("uid");
				String username = resultSet.getString("username");
				String photoUri = resultSet.getString("photo_uri");

				user = new Profile( username, photoUri,userId );
				lastUserId = userId;
			}
		}catch (SQLException ex){
			System.out.println(ex);
		}
		 return user;
	}
	public int                getUsersCount(){
		int userCount = 0;
		String query = "SELECT COUNT(*) FROM users";
		try (Statement statement = conn.createStatement()) {
			ResultSet resultSet = statement.executeQuery(query);
			if (resultSet.next()) {
				userCount = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userCount;
	}

	public Integer            LogIn(String email, String password){
		Integer id = null;
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT uid FROM users WHERE email = ? and password =?");
			stmt.setString(1, email);
			stmt.setString(2, password);
			ResultSet profileResult = stmt.executeQuery();

			if (profileResult.next()) {
				id = profileResult.getInt("uid");
			}

		}catch (Exception ex){
			System.out.println(ex);
		}
		return id;
	}

	public void setLastUserId(int lastUserId) {
		this.lastUserId = lastUserId;
	}
}
