package org.controllers;

import org.entities.Profile;

import java.util.ArrayList;
import java.util.Objects;

public class ProfilesDB {
	ArrayList<Profile> db = new ArrayList<>();
	ArrayList<Profile> liked = new ArrayList<>();

	public void                 initDB(){
		db.add(new Profile("Walter",  "https://openseauserdata.com/files/05a9e4e540ab76d4b8b3bfcceadaec0a.jpg",       1, "heisenberg@example.com",     "BlueSky123"));
		db.add(new Profile("Tyler",   "https://i1.sndcdn.com/artworks-175OxlMgu7FN1SrE-yr9gbQ-t500x500.jpg",          2, "tyler.d@example.com",        "FightClub99"));
		db.add(new Profile("Patrick", "https://www.apetogentleman.com/wp-content/uploads/2015/10/patrickbateman.jpg", 3, "americanpsycho@example.com", "Chainsaw1"));
		db.add(new Profile("Travis",  "https://anygoodfilms.com/wp-content/uploads/2016/08/taxi-driver.jpg",          4, "travisb@example.com",        "LonelyN1ghts"));
	}
	public Profile              getOne(int index){
		return db.get(index);
	}
	public Profile              getById(int i){
		return db.stream().filter(p -> p.getId() ==i).findFirst().orElse(null);
	}
	public Profile              getByName(String name){
		return db.stream().filter(p -> Objects.equals(p.getName(), name)).findFirst().orElse(null);
	}
	public ArrayList<Profile>   getAll(){
		return db;
	}
	public boolean              like(Profile p){
		try {
			liked.add(p);
			return true;
		}catch (Exception ex){
			return false;
		}
	}
	public ArrayList<Profile>   getLiked(){
		return liked;
	}
}
