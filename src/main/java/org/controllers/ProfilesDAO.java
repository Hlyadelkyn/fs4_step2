package org.controllers;

import org.entities.Profile;

import java.util.ArrayList;

public class ProfilesDAO {
	public ProfilesDAO(ProfilesDB pdb){
		this.db =pdb;
	}
	int counter = 0;
	ProfilesDB db;
	public Profile getNext(){
		if(counter >= 3){
			counter = 0;
		}else{
			++counter;
		}
		return db.getOne(counter);
	}
	public ArrayList<Profile>        getAll(){
		return db.getAll();
	}

	public ArrayList<Profile>        getLiked(){
		return db.getLiked();
	}
	public boolean                   like(Profile p){
		return db.like(p);

	}

	public Profile                   getByName(String name){
		return db.getByName(name);
	}
	public Profile                   getById(int id){
		return db.getById(id);
	}
}
