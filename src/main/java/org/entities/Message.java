package org.entities;

import java.sql.Timestamp;

public class Message {
	int id;
	int sentId;
	int receivedId;
	Timestamp timeStamp;
	String text;

	public Message(int mi, int sid, int rid,  String text, Timestamp t){
		id = mi;
		sentId = sid;
		receivedId = rid;
		this.text = text;
		timeStamp = t;
	}

	public int getId() {
		return id;
	}

	public int getReceivedId() {
		return receivedId;
	}

	public int getSentId() {
		return sentId;
	}

	public Timestamp getTimeStamp() {
		return timeStamp;
	}

	public String getText() {
		return text;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setReceivedId(int receivedId) {
		this.receivedId = receivedId;
	}

	public void setSentId(int sentId) {
		this.sentId = sentId;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}
}

