package org.entities;

import java.util.ArrayList;

public class Profile {
	String name;
	String photoWebURL;
	private int id;

	private String email;

	private String password;

	public Profile(String name, String url, int i) {
		setName(name);
		setPhotoWebURL(url);
		setId(i);
	}
	public Profile(String name, String url, int i, String e, String p) {
		setName(name);
		setPhotoWebURL(url);
		setId(i);
		setPassword(p);
		setEmail(e);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhotoWebURL(String photoWebURL) {
		this.photoWebURL = photoWebURL;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public String getPhotoWebURL() {
		return photoWebURL;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString(){
		return name + " | id "+id;
	}



}
