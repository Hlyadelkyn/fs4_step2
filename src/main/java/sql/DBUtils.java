package sql;

import org.controllers.ProfilesDAO;
import org.controllers.SQLDAO;
import org.entities.Profile;

import java.util.ArrayList;

public class DBUtils {
	ProfilesDAO   PDAO;
	SQLDAO        SDAO;

	public DBUtils(ProfilesDAO p, SQLDAO s){
		PDAO = p;
		SDAO = s;
	}
	public void copyLocalUsers(){
		System.out.println("in process...");
		ArrayList<Profile> profiles = PDAO.getAll();
		System.out.println(profiles);
		for(Profile p : profiles){
			SDAO.addUser(p);
		}
	}
	public void init(){
		copyLocalUsers();
	}

}
